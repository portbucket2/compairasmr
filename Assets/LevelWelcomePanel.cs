﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelWelcomePanel : MonoBehaviour
{
    public Text levelNum;
    // Start is called before the first frame update
    void Start()
    {
        LevelWelcomeTextDisappear();
        GameStatesControl.instance.actionLevelStart += LevelWelcomeTextAppear;
        GameStatesControl.instance.actionGameplay += LevelWelcomeTextDisappear;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LevelWelcomeTextAppear()
    {
        levelNum.text = "LEVEL " + (GameManagement.instance.levelIndexDisplayed + 1);
        levelNum.gameObject.SetActive(true);

        levelNum.GetComponent<UiTransitionCustom>().TransitionRightOrLeftToCenter(-2000f, 0,0,  3,  null, false);

        //LevelWelcomeTextDisappearDelayed(2f);
    }
    public void LevelWelcomeTextDisappear()
    {
        //levelNum.gameObject.SetActive(false);
        levelNum.GetComponent<UiTransitionCustom>().TransitionRightOrLeftToCenter(0, 2000f, 0, 3, null, true);
    }
    public void LevelWelcomeTextDisappearDelayed(float t)
    {
        Invoke("LevelWelcomeTextDisappear", t);
    }
}
