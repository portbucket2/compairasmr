﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagement : MonoBehaviour
{
    public static GameManagement instance;
    public int levelIndexDisplayed;
    [SerializeField]
    private int levelIndex;
    [SerializeField]
    private int maxLevelIndex;



    private void Awake()
    {
        instance = this;
        levelIndexDisplayed = PlayerPrefs.GetInt("levelIndexDisplayed", levelIndexDisplayed);
        levelIndex = levelIndexDisplayed % (maxLevelIndex + 1);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public int GetLevelIndex()
    {
        return levelIndex;
    }

    public void NextLevelValueUpdate()
    {
        levelIndexDisplayed += 1;
        levelIndex = levelIndexDisplayed % (maxLevelIndex+ 1);
        //levelIndex += 1;
        if(levelIndex > maxLevelIndex)
        {
            //levelIndex = 0;
        }
        PlayerPrefs.SetInt("levelIndexDisplayed", levelIndexDisplayed);
    }

    public void DisablePainting()
    {
        PaintCompletionDetection.instance.initialized = false;
        PaintCompletionDetection.instance.cleaningFac = 0;
        DirtMaskHandler.instance.initialized = false;
    }

    public void LevelStartedAnalyticsCall()
    {
        int level = (levelIndexDisplayed + 1);
        Debug.Log("Level Started " + level);
        LionStudios.Analytics.Events.LevelStarted(level,0);
    }
    public void LevelCompletedAnalyticsCall()
    {
        int level = (levelIndexDisplayed + 1);
        Debug.Log("Level Completed " + level);
        LionStudios.Analytics.Events.LevelComplete(level, 0);
    }
}
