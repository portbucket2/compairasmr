﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshDataExtractor : MonoBehaviour
{
    public GameObject meshObj;
    public Mesh mesh;
    public Vector3[] meshVertices;
    public Vector2[] meshUvs;
    public int[] meshTris;

    public GameObject inddicator;

    public List<TriagleIndexCustom> trisIndices;

    public List<Idicator> indicatorList;

    [System.Serializable]
    public class TriagleIndexCustom
    {
        public int index;
        public Vector3Int vertsInside;
    }
    // Start is called before the first frame update
    void Start()
    {
        mesh = meshObj.GetComponent<MeshFilter>().sharedMesh;
        meshVertices = mesh.vertices;
        meshTris = mesh.triangles;
        meshUvs = mesh.uv;

        TriangleIndexesDetermine();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TriangleIndexesDetermine()
    {
        int index = 0;
        for (int i = 0; i < meshTris.Length; i++)
        {
            if(i%3 == 0)
            {
                TriagleIndexCustom t = new TriagleIndexCustom();
                t.index = index;
                t.vertsInside = new Vector3Int(meshTris[i], meshTris[i + 1], meshTris[i + 2]);
                trisIndices.Add(t);

                Vector3 pt = AverageIt(meshVertices[t.vertsInside.x], meshVertices[t.vertsInside.y], meshVertices[t.vertsInside.z]);
                //if(index == 0)
                GameObject go =  Instantiate(inddicator, pt, transform.rotation);
                go.GetComponent<Idicator>().index = index;
                indicatorList.Add(go.GetComponent<Idicator>());
                index += 1;

            }
        }
    }

    public Vector3 AverageIt(Vector3 a , Vector3 b, Vector3 c)
    {
        Vector3 d = Vector3.Lerp(a, b, 0.5f);
        return Vector3.Lerp(d, c, 0.33f);
    }
}
