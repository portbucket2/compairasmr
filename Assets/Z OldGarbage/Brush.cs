﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brush : MonoBehaviour
{
    public float range;
    public MeshDataExtractor meshDataExtractor;
    TextureMaker textureMaker;

    public Texture2D texBrush;
    public Color[] brushColorArray;

    public Color c;
    public Vector2Int v;
    public int brushPixels;

    public GameObject indicatorVert;
    
    // Start is called before the first frame update
    void Start()
    {
        //brushColorArray = texBrush.GetPixels();
        brushPixels = texBrush.width;
        c = texBrush.GetPixel(v.x, v.y);
        textureMaker =meshDataExtractor. GetComponent<TextureMaker>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.V))
        {
            IndicatorsAtRange();
        }
        c = texBrush.GetPixel(v.x, v.y);
    }

    public void IndicatorsAtRange()
    {
        //int t = 0;
        for (int i = 0; i < meshDataExtractor.indicatorList.Count; i++)
        {
            Vector3 p = transform.InverseTransformPoint(meshDataExtractor.indicatorList[i].transform.position);
            if (Mathf.Abs( p.x )< range && Mathf.Abs(p.y) < range && Mathf.Abs(p.z) < range*0.5f)
            {
                meshDataExtractor.indicatorList[i].IndicatorColor();
                textureMaker.triangleCustom.cornerColors[0] = PixelColorValueInrespectOfBrush(meshDataExtractor.meshVertices[meshDataExtractor.trisIndices[i].vertsInside[0]]);
                textureMaker.triangleCustom.cornerColors[1] = PixelColorValueInrespectOfBrush(meshDataExtractor.meshVertices[meshDataExtractor.trisIndices[i].vertsInside[1]]);
                textureMaker.triangleCustom.cornerColors[2] = PixelColorValueInrespectOfBrush(meshDataExtractor.meshVertices[meshDataExtractor.trisIndices[i].vertsInside[2]]);
                textureMaker.ColorTheTrisOfIndex(meshDataExtractor.indicatorList[i].index);
                //if(t == 0){
                //    Instantiate(indicatorVert, meshDataExtractor.meshVertices[meshDataExtractor.trisIndices[i].vertsInside[0]], transform.rotation);
                //    t += 1;
                //}
                

            }
            else
            {
                meshDataExtractor.indicatorList[i].IndicatorDisColor();
            }
        }
    }

    public Color PixelColorValueInrespectOfBrush(Vector3 vert)
    {
        //Color c = Color.white;
        Vector3 localP = transform.InverseTransformPoint(vert);
        float a = Mathf.Clamp( ((range * 0.5f) + localP.x) / range , 0 ,1);
        float b = Mathf.Clamp(((range * 0.5f) + localP.y) / range, 0, 1);

        Vector2Int v = new Vector2Int((int)(a * brushPixels), (int)(b * brushPixels));
        Color col = texBrush.GetPixel(v.x , v.y);

        //Debug.Log(localP +",,"+a + "," + b + ",," + v);
        return col;
    }
}
