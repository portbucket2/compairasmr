﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureMaker : MonoBehaviour
{
    //public Texture2D myTex;
    public Texture2D tex;
    public Material mat;
    public Color[] colorArray;
    public int texWidth;
    public GameObject[] tr;
    public GameObject pointer;
    public Vector3 point;
    public bool inside;
    public float a;
    public float b;
    public float c;
    public bool IsInside;
    int coloringIndex;
    public TriangleIndexCustom triangleCustom;
    MeshDataExtractor meshDataExtractor;

    [System.Serializable]
    public class TriangleIndexCustom
    {
        public Texture2D tex;
        public Color[] colorArray;
        public int texWidth;
        public int index;
        public int[] cornerPoints = new int[3];
        public Vector2[] cornerUvs = new Vector2[3];
        public Color[] cornerColors = new Color[3];
        public List<int> pixelsIndexes;
        public List<Vector3> colorShareValues;

        public void DetectTriangleAreaInUv()
        {
            //DetectAnArea(new Vector2(0.1f, 0.1f), new Vector2(.5f, .8f), new Vector2(1f, 0f));
        }

        public void ApplyColorAsShared()
        {
            for (int i = 0; i < pixelsIndexes.Count; i++)
            {
                //Debug.Log(colorArray[pixelsIndexes[i]]);
                //Color c = (cornerColors[0] * colorShareValues[i].x) + (cornerColors[1] * colorShareValues[i].y) + (cornerColors[2] * colorShareValues[i].z);
                //cornerColors[0] = Color.Lerp(colorArray[pixelsIndexes[i]], cornerColors[0], cornerColors[0].a);
                Color c =Color.Lerp(  cornerColors[0], cornerColors[1], colorShareValues[i].x/(colorShareValues[i].x+ colorShareValues[i].y));
                Color d = Color.Lerp(cornerColors[2],c, colorShareValues[i].z/((colorShareValues[i].x + colorShareValues[i].y+ colorShareValues[i].z)*0.5f) );
                //Color v = Colo
                float a = d.a;
                Color b = colorArray[pixelsIndexes[i]];
                colorArray[pixelsIndexes[i]] = Color.Lerp(b, d, a);
            }
            //tex.filterMode = FilterMode.Bilinear;
            tex.SetPixels(colorArray);
            tex.Apply();

            //tex.filterMode = FilterMode.Trilinear;

            
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        meshDataExtractor = GetComponent<MeshDataExtractor>();

        CreateTexture();

        //triangleCustom.tex = tex;
        //triangleCustom.colorArray = colorArray;
        //triangleCustom.texWidth = texWidth;
        //
        ////triangleCustom.cornerUvs[0] = new Vector2(0.1f, 0.1f);
        ////triangleCustom.cornerUvs[1] = new Vector2(.5f, .8f);
        ////triangleCustom.cornerUvs[2] = new Vector2(1f, 0f);
        //
        //DetectAnArea(triangleCustom.cornerUvs[0], triangleCustom.cornerUvs[1], triangleCustom.cornerUvs[2]);
        //
        //triangleCustom.ApplyColorAsShared();
    }

    // Update is called once per frame
    void Update()
    {
        //IsInside =  InsideOrNot();
        if (Input.GetKey(KeyCode.C))
        {
            triangleCustom.tex = tex;
            triangleCustom.colorArray = colorArray;
            triangleCustom.texWidth = texWidth;

            //triangleCustom.cornerUvs[0] = new Vector2(0.1f, 0.1f);
            //triangleCustom.cornerUvs[1] = new Vector2(.5f, .8f);
            //triangleCustom.cornerUvs[2] = new Vector2(1f, 0f);


            triangleCustom.cornerUvs[0] = meshDataExtractor.meshUvs[meshDataExtractor.trisIndices[coloringIndex].vertsInside.x];
            triangleCustom.cornerUvs[1] = meshDataExtractor.meshUvs[meshDataExtractor.trisIndices[coloringIndex].vertsInside.y];
            triangleCustom.cornerUvs[2] = meshDataExtractor.meshUvs[meshDataExtractor.trisIndices[coloringIndex].vertsInside.z];

            DetectAnArea(triangleCustom.cornerUvs[0], triangleCustom.cornerUvs[1], triangleCustom.cornerUvs[2]);

            triangleCustom.ApplyColorAsShared();

            coloringIndex += 1;
        }
    }

    public void ColorTheTrisOfIndex(int coloringIndex)
    {
        triangleCustom.tex = tex;
        triangleCustom.colorArray = colorArray;
        triangleCustom.texWidth = texWidth;

        //triangleCustom.cornerUvs[0] = new Vector2(0.1f, 0.1f);
        //triangleCustom.cornerUvs[1] = new Vector2(.5f, .8f);
        //triangleCustom.cornerUvs[2] = new Vector2(1f, 0f);


        triangleCustom.cornerUvs[0] = meshDataExtractor.meshUvs[meshDataExtractor.trisIndices[coloringIndex].vertsInside.x];
        triangleCustom.cornerUvs[1] = meshDataExtractor.meshUvs[meshDataExtractor.trisIndices[coloringIndex].vertsInside.y];
        triangleCustom.cornerUvs[2] = meshDataExtractor.meshUvs[meshDataExtractor.trisIndices[coloringIndex].vertsInside.z];

        DetectAnArea(triangleCustom.cornerUvs[0], triangleCustom.cornerUvs[1], triangleCustom.cornerUvs[2]);

        triangleCustom.ApplyColorAsShared();

        //coloringIndex += 1;
    }

    public void CreateTexture()
    {
        tex = new Texture2D(texWidth, texWidth);

        colorArray = new Color[texWidth* texWidth];

        for (int i = 0; i < texWidth; i++)
        {
            for (int j = 0; j < texWidth; j++)
            {
               //if(j > texWidth / 2)
               //{
               //    colorArray[(i * texWidth) + j] = Color.red;
               //    //colorArray[(i * texWidth) + j].a = 0f;
               //}
               //else
               //{
               //    colorArray[(i * texWidth) + j] = Color.blue;
               //}
                colorArray[(i * texWidth) + j] = Color.black;
                //Debug.Log(colorArray[(i * texWidth) + j]);
            }
        }

        //for (int i = 0; i < colorArray.Length; i++)
        //{
        //    colorArray[i] = Color.red;
        //}

        tex.SetPixels(colorArray);
        tex.Apply();

        mat.SetTexture("_MainTex",tex);
        
    }


    public void DetectAnArea(Vector2 point1 , Vector2 point2, Vector2 point3)
    {
        Vector2[] points = new Vector2[3];
        points[0] = point1;
        points[1] = point2;
        points[2] = point3;
        Vector2 minPoint = new Vector2(1, 1);
        Vector2 maxPoint = new Vector2(0, 0);
        for (int i = 0; i < points.Length; i++)
        {
            if(points[i].x <= minPoint.x)
            {
                minPoint.x = points[i].x;
            }
            if (points[i].x >= maxPoint.x)
            {
                maxPoint.x = points[i].x;
            }
            if (points[i].y <= minPoint.y)
            {
                minPoint.y = points[i].y;
            }
            if (points[i].y >= maxPoint.y)
            {
                maxPoint.y = points[i].y;
            }
        }
        //Debug.Log(minPoint);
        //Debug.Log(maxPoint);

        Vector2 min = new Vector2((int) (minPoint.x * texWidth),(int)(minPoint.y * texWidth));
        Vector2 max = new Vector2(Mathf.CeilToInt(maxPoint.x * texWidth), Mathf.CeilToInt(maxPoint.y * texWidth));

        List<int> pixelsSelected = new List<int>();
        List<Vector3> colorShareValues = new List<Vector3>() ;

        for (int i = (int)min.y; i < (int)max.y; i++)
        {
            for (int j = (int)min.x; j < (int)max.x; j++)
            {
                //colorArray[(i * texWidth) + j] = Color.green;
                //pixelsSelected.Add((i * texWidth) + j);
                Vector2 v = new Vector2( (float)j / (float)texWidth , (float)i / (float)texWidth);
                bool b = InsideOrNot(point1, point2, point3, v);
                if (b)
                {
                    //colorArray[(i * texWidth) + j] = Color.green;
                    pixelsSelected.Add((i * texWidth) + j);
                    float d1 = Vector2.Distance(v, point1);
                    float d2 = Vector2.Distance(v, point2);
                    float d3 = Vector2.Distance(v, point3);
                    float d = d1 + d2 + d3;
                    Vector3 v3 = new Vector3(d1,d2,d3);

                    colorShareValues.Add(v3);

                }
                //Debug.Log((float)i/ (float)texWidth +" "+ (float)j / (float)texWidth);
            }
        }

        triangleCustom.pixelsIndexes = pixelsSelected;
        triangleCustom.colorShareValues = colorShareValues;

        tex.SetPixels(colorArray);
        tex.Apply();

        
    }

    

    public bool InsideOrNot(Vector2 vert1, Vector2 vert2, Vector2 vert3, Vector2 point)
    {
        a = 0;
        b = 0;
        c = 0;
        //Vector2 x = new Vector2( tr[0].transform.position.x, tr[0].transform.position.z);
        //Vector2 y = new Vector2(tr[1].transform.position.x, tr[1].transform.position.z);
        //Vector2 z = new Vector2(tr[2].transform.position.x, tr[2].transform.position.z);
        //
        //Vector2 p = new Vector2(pointer.transform.position.x, pointer.transform.position.z);
        Vector2 x = vert1;
        Vector2 y = vert2;
        Vector2 z = vert3;

        Vector2 p = point;

        //point = Vector3.Cross(y - x, p - x);
        //b = Vector3.Cross(y - x, p - x).y;
        a = Vector3.Cross(y - x, p - x).z;
        b = Vector3.Cross(z - y, p - y).z;
        c = Vector3.Cross(x - z, p - z).z;

        //a = Vector3.Cross((tr[1].transform.position - tr[0].transform.position), (pointer.transform.position - tr[0].transform.position)).y;
        //b = Vector3.Cross((tr[2].transform.position - tr[1].transform.position), (pointer.transform.position - tr[1].transform.position)).y;
        //c = Vector3.Cross((tr[0].transform.position - tr[2].transform.position), (pointer.transform.position - tr[2].transform.position)).y;

        

        if(a <= 0 && b <= 0 && c <= 0)
        {
            return true;
        }
        
        else
        {
            return  false;
        }

    }
}
