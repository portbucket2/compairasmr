﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrinagleIndexCustom : TextureMaker
{
    //public Texture2D tex ;
    //public Color[] colorArray;
    //public int texWidth;
    public int index;
    public int[] cornerPoints = new int[3];
    public Vector2[] cornerUvs = new Vector2[3];
    public Color[] cornerColors = new Color[3];
    public List<int> pixelsIndexes;
    public List<Vector3> colorShareValues;

   

    public void ApplyColorAsShared()
    {
        for (int i = 0; i < pixelsIndexes.Count; i++)
        {
            Color c = Color.Lerp(cornerColors[0], cornerColors[1], colorShareValues[i].x / (colorShareValues[i].x + colorShareValues[i].y));
            Color d = Color.Lerp(cornerColors[2], c, colorShareValues[i].z / ((colorShareValues[i].x + colorShareValues[i].y + colorShareValues[i].z) * 0.5f));
            
            colorArray[pixelsIndexes[i]] = d;
        }
        tex.SetPixels(colorArray);
        tex.Apply();

    }
        // Start is called before the first frame update
        void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void DetectAnArea(Vector2 point1, Vector2 point2, Vector2 point3)
    {
        Vector2[] points = new Vector2[3];
        points[0] = point1;
        points[1] = point2;
        points[2] = point3;
        Vector2 minPoint = new Vector2(1, 1);
        Vector2 maxPoint = new Vector2(0, 0);
        for (int i = 0; i < points.Length; i++)
        {
            if (points[i].x <= minPoint.x)
            {
                minPoint.x = points[i].x;
            }
            if (points[i].x >= maxPoint.x)
            {
                maxPoint.x = points[i].x;
            }
            if (points[i].y <= minPoint.y)
            {
                minPoint.y = points[i].y;
            }
            if (points[i].y >= maxPoint.y)
            {
                maxPoint.y = points[i].y;
            }
        }
        Debug.Log(minPoint);
        Debug.Log(maxPoint);

        Vector2 min = new Vector2((int)(minPoint.x * texWidth), (int)(minPoint.y * texWidth));
        Vector2 max = new Vector2((int)(maxPoint.x * texWidth), (int)(maxPoint.y * texWidth));

        List<int> pixelsSelected = new List<int>();
        List<Vector3> colorShareValues = new List<Vector3>();

        for (int i = (int)min.y; i < (int)max.y; i++)
        {
            for (int j = (int)min.x; j < (int)max.x; j++)
            {
                
                Vector2 v = new Vector2((float)j / (float)texWidth, (float)i / (float)texWidth);
                bool b = InsideOrNot(point1, point2, point3, v);
                if (b)
                {
                    //colorArray[(i * texWidth) + j] = Color.green;
                    pixelsSelected.Add((i * texWidth) + j);
                    float d1 = Vector2.Distance(v, point1);
                    float d2 = Vector2.Distance(v, point2);
                    float d3 = Vector2.Distance(v, point3);
                    float d = d1 + d2 + d3;
                    Vector3 v3 = new Vector3(d1, d2, d3);

                    colorShareValues.Add(v3);

                }
               
            }
        }

        triangleCustom.pixelsIndexes = pixelsSelected;
        triangleCustom.colorShareValues = colorShareValues;

        tex.SetPixels(colorArray);
        tex.Apply();
    }



    public bool InsideOrNot(Vector2 vert1, Vector2 vert2, Vector2 vert3, Vector2 point)
    {
        a = 0;
        b = 0;
        c = 0;
        
        Vector2 x = vert1;
        Vector2 y = vert2;
        Vector2 z = vert3;

        Vector2 p = point;

        a = Vector3.Cross(y - x, p - x).z;
        b = Vector3.Cross(z - y, p - y).z;
        c = Vector3.Cross(x - z, p - z).z;


        if (a < 0 && b < 0 && c < 0)
        {
            return true;
        }

        else
        {
            return false;
        }

    }
}
