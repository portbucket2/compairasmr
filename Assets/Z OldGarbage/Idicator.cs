﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Idicator : MonoBehaviour
{
    public int index;
    public Color colorNonSelected;
    public Color coloSelected;
    Renderer r;
    // Start is called before the first frame update
    void Start()
    {
        r = GetComponentInChildren<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void IndicatorColor()
    {
        r.material.color = coloSelected;
    }
    public void IndicatorDisColor()
    {
        r.material.color = colorNonSelected;
    }
}
