﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaskToFocus : MonoBehaviour
{
    public Image maskImage;
    public Vector3 maskSize;
    public GameObject refCube;
    public GameObject[] refLevelCubes;
    public GameObject rightImage;
    public GameObject LeftImage;
    public GameObject upImage;
    public GameObject downImage;
    public Color maskColor;
    public Color transColor;

    public Image[] imagesInside;
    public float colorFac;
    float tarFAc;
    // Start is called before the first frame update
    void Start()
    {
        GameStatesControl.instance.actionGameplay += MaskFocus;
        GameStatesControl.instance.actionLevelResult += MaskDeFocus;

        for (int i = 0; i < imagesInside.Length; i++)
        {
            imagesInside[i].color = transColor;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        if(colorFac != tarFAc)
        {
            colorFac = Mathf.MoveTowards(colorFac,tarFAc, Time.deltaTime*3);
            for (int i = 0; i < imagesInside.Length; i++)
            {
                imagesInside[i].color = Color.Lerp(transColor, maskColor, colorFac);
            }

        }

    }

    public void MaskFocus()
    {
        tarFAc = 1;
        refCube = refLevelCubes[GameManagement.instance.GetLevelIndex()];
        maskSize = refCube.transform.localScale;
        maskImage.gameObject.transform.localScale = new Vector3(maskSize.x, maskSize.z, 0);
        maskImage.gameObject.transform.position = refCube.transform.position + new Vector3(0, 0.2f, 0);
        rightImage.transform.position = maskImage.gameObject.transform.position + new Vector3(-maskImage.gameObject.transform.localScale.x * 0.5f, 0, 0);
        LeftImage.transform.position = maskImage.gameObject.transform.position + new Vector3(maskImage.gameObject.transform.localScale.x * 0.5f, 0, 0);

        upImage.transform.position = maskImage.gameObject.transform.position + new Vector3(0, 0, -maskImage.gameObject.transform.localScale.y * 0.5f);
        downImage.transform.position = maskImage.gameObject.transform.position + new Vector3(0, 0, maskImage.gameObject.transform.localScale.y * 0.5f);

        rightImage.transform.localScale = new Vector3(rightImage.transform.localScale.x, maskImage.gameObject.transform.localScale.y, rightImage.transform.localScale.z);
        LeftImage.transform.localScale = new Vector3(LeftImage.transform.localScale.x, maskImage.gameObject.transform.localScale.y, LeftImage.transform.localScale.z);
    }
    public void MaskDeFocus()
    {
        tarFAc = 0;

        //maskSize = refCube.transform.localScale;
        //maskImage.gameObject.transform.localScale = new Vector3(maskSize.x, maskSize.z, 0);
        //maskImage.gameObject.transform.position = refCube.transform.position + new Vector3(0, 0.1f, 0);
        //rightImage.transform.position = maskImage.gameObject.transform.position + new Vector3(-maskImage.gameObject.transform.localScale.x * 0.5f, 0, 0);
        //LeftImage.transform.position = maskImage.gameObject.transform.position + new Vector3(maskImage.gameObject.transform.localScale.x * 0.5f, 0, 0);
        //
        //upImage.transform.position = maskImage.gameObject.transform.position + new Vector3(0, 0, -maskImage.gameObject.transform.localScale.y * 0.5f);
        //downImage.transform.position = maskImage.gameObject.transform.position + new Vector3(0, 0, maskImage.gameObject.transform.localScale.y * 0.5f);
        //
        //rightImage.transform.localScale = new Vector3(rightImage.transform.localScale.x, maskImage.gameObject.transform.localScale.y, rightImage.transform.localScale.z);
        //LeftImage.transform.localScale = new Vector3(LeftImage.transform.localScale.x, maskImage.gameObject.transform.localScale.y, LeftImage.transform.localScale.z);
    }
}
