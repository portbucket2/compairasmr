﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirtMaskHandler : MonoBehaviour
{
    public GameObject invisibleObj;
    //public Material mat;
    public Texture texWritten;
    public Material matVisualMeshCustom;
    //public Material matMaskRT;
    public static DirtMaskHandler instance;

    public float mouseOffset;


    public bool initialized;

    public Material testMat;
    private void Awake()
    {
        instance = this;
        mouseOffset = Screen.height / 10;
    }
    // Start is called before the first frame update
    void Start()
    {
        //mat = invisibleObj.GetComponent<Renderer>().material;
        //texWritten = mat.GetTexture("_MainTex");

    }

    // Update is called once per frame
    void Update()
    {
        if (!initialized)
        {
            return;
        }
        
        //mat = invisibleObj.GetComponent<Renderer>().material;
        texWritten = invisibleObj.GetComponent<Renderer>().material.GetTexture("_MainTex");

        matVisualMeshCustom.SetTexture("Texture2D_50D22C30", texWritten);
        //matMaskRT.SetTexture("Texture2D_21E5785C", texWritten);

        testMat.mainTexture = texWritten;
    }

    public void InitialTasks()
    {
        initialized = true;
    }
}
