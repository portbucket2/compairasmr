﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPositionsOnLevels : MonoBehaviour
{
    public GameObject camPosRefsRoot;
    public GameObject[] camPosRefs;
    public CameraZoomAndMove cameraZoomAndMove;
    // Start is called before the first frame update
    void Start()
    {
        GameStatesControl.instance.actionGameplay += AssignRightCamPosRef;
        GameStatesControl.instance.actionLevelStart += AssignRightCamPosRefOfRoot;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AssignRightCamPosRef()
    {

        cameraZoomAndMove.camPosRef = camPosRefs[GameManagement.instance.GetLevelIndex()];
        cameraZoomAndMove.tarPos = cameraZoomAndMove.camPosRef.transform.position;
    }

    public void AssignRightCamPosRefOfRoot()
    {

        cameraZoomAndMove.camPosRef = camPosRefsRoot;
        cameraZoomAndMove.tarPos = cameraZoomAndMove.camPosRef.transform.position;
    }
}
