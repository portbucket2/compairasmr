﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameStatesControl : MonoBehaviour
{
    [SerializeField]
    private GameStates gameState;
    public Action actionMenuIdle   ;
    public Action actionGameStart  ;
    public Action actionLevelStart ;
    public Action actionGameplay   ;
    public Action actionLevelResult;
    public Action actionLevelEnd    ;
    public Action actionCallNextLevel;

    public static GameStatesControl instance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        ActionFuntionsAssignment();

        GameStateChangeTo(GameStates.MenuIdle);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameStates GetGameStateCurrent()
    {
        return gameState;
    }
    public void GameStateChangeToDelayed(GameStates state , float t)
    {
        StopAllCoroutines();
        StartCoroutine(GameStateChangeCoRoutine(state, t));
    }
    public IEnumerator GameStateChangeCoRoutine(GameStates state, float t)
    {
        yield return new WaitForSeconds(t);
        GameStateChangeTo(state);

    }
    public void GameStateChangeTo(GameStates state)
    {
        gameState = state;
        switch (gameState)
        {
            case GameStates.MenuIdle:
                {
                    actionMenuIdle   ?.Invoke();
                    break;
                }
            case GameStates.GameStart:
                {
                    actionGameStart?.Invoke();
                    GameStatesControl.instance.GameStateChangeTo(GameStates.LevelStart);
                    break;
                }
            case GameStates.LevelStart:
                {
                    actionLevelStart?.Invoke();
                    LevelManager.instance.LevelStart();
                    GameStatesControl.instance.GameStateChangeToDelayed(GameStates.Gameplay, 1.5f);
                    break;
                }
            case GameStates.Gameplay:
                {
                    actionGameplay?.Invoke();
                    break;
                }

            case GameStates.LevelResult:
                {
                    actionLevelResult?.Invoke();
                    GameManagement.instance.DisablePainting();
                    GameStateChangeToDelayed(GameStates.LevelEnd,1f);
                    break;
                }
            case GameStates.LevelEnd:
                {
                    actionLevelEnd?.Invoke();
                    break;
                }
            case GameStates.CallNextLevel:
                {
                    actionCallNextLevel?.Invoke();
                    GameManagement.instance.NextLevelValueUpdate();

                    GameStatesControl.instance.GameStateChangeTo(GameStates.LevelStart);
                    break;
                }
        }
    }

    public void ActionFuntionsAssignment()
    {
        actionMenuIdle    += UiManage.instance.FunUiMenuIdle   ;


        actionGameStart   += UiManage.instance.FunUiGameStart  ;


        actionLevelStart  += UiManage.instance.FunUiLevelStart ;
        actionLevelStart += GameManagement.instance.LevelStartedAnalyticsCall;
        //actionLevelStart += InputMouseSystem.instance.TouchEnable;


        actionGameplay    += UiManage.instance.FunUiGameplay   ;
        actionGameplay += InputMouseSystem.instance.TouchEnable;


        actionLevelResult += UiManage.instance.FunUiLevelResult;
        actionLevelResult += InputMouseSystem.instance.TouchDisable;
        actionLevelResult += GameManagement.instance.LevelCompletedAnalyticsCall;


        actionLevelEnd +=    UiManage.instance.FunUiLevelEnd;



    }
}
