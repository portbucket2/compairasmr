﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointerCircle : MonoBehaviour
{
    public Canvas canvasMain;
    float scale;
    public float scaleMax;
    public float scaleMin;
    //public Color col;
    //public Vector2 refRes;
    //public Vector2 screen;
    //public Vector2 MousePos;
    //public Vector2 pointerPos;
    // Start is called before the first frame update
    void Start()
    {
        //refRes = canvasMain.GetComponent<CanvasScaler>().referenceResolution;
        //screen.x = Screen.width;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (InputMouseSystem.instance.MouseTappedOrNot())
        {
            PointerAllignToMouse();
            scale = scaleMax;
            
        }
        else
        {
            scale = scaleMin;
        }
        transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(scale, scale, scale), Time.deltaTime * 5);
        //col.a =scaleMin-  scale;
    }

    public void PointerAllignToMouse()
    {
        //MousePos = Input.mousePosition;
        //pointerPos.x = ((float)Input.mousePosition.x / (float)Screen.width) * (float)refRes.x - (refRes.x * 0f);
        //float y= (Input.mousePosition.y / Screen.height) * refRes.y - (refRes.y * 0f);
        transform.position = Input.mousePosition + new Vector3(0, DirtMaskHandler.instance.mouseOffset);
    }
}
