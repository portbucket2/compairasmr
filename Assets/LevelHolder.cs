﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelHolder : MonoBehaviour
{
    public GameObject meshToBePainted;
    public GameObject meshDummyVisual;
    public Material matToBePainted;
    public Material matOutputCustom;
    public Texture2D texClean;
    public Texture2D texDirty;
    public Texture2D texMask;
    public Texture2D whiteTex;

    public GameObject waverDirt;
    public Texture2D dirtLevelMask;
    public GameObject[] waverDirtPosRefs;
    public List<GameObject> generatedWavers;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InitializeLevel()
    {
        DirtMaskHandler.instance.invisibleObj = meshToBePainted;
        DirtMaskHandler.instance.matVisualMeshCustom = matOutputCustom;

        PaintCompletionDetection.instance.maskUv = texMask;

        PaintCompletionDetection.instance.InitialTasks();
        DirtMaskHandler.instance.InitialTasks();


        Graphics.Blit(whiteTex, (RenderTexture)meshToBePainted.GetComponent<Renderer>().material.mainTexture);

        matOutputCustom.SetTexture("Texture2D_B387B4DE", dirtLevelMask);
        //Material mat = new Material(matToBePainted);
        //meshToBePainted.GetComponent<Renderer>().material = mat;
        GenerateWaverDirts();
    }
    public void GenerateWaverDirts()
    {
        for (int i = 0; i < generatedWavers.Count; i++)
        {
            if(generatedWavers[i] != null)
            {
                Destroy(generatedWavers[i].gameObject);
            }
        }
        generatedWavers.Clear();

        for(int i = 0; i < waverDirtPosRefs. Length; i++)
        {
            GameObject go = Instantiate(waverDirt, waverDirtPosRefs[i].transform.position, waverDirtPosRefs[i].transform.rotation);
            go.transform.localScale = Vector3.one * 0.1f * Random.Range(.1f, 0.8f);
            generatedWavers.Add(go);

        }
    }
}
