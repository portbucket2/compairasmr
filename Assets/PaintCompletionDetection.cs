﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PaintCompletionDetection : MonoBehaviour
{
    //public Texture tex;
    public Texture2D tex2dTest;
    //public Material matTest;
    //public Texture2D tex2d;
    //public RenderTexture RT;
    //public Camera RTCAM;
    public Color[] colorArray;
    DirtMaskHandler dirtMaskHandler;
    //public Material maskMat;

    public List<int> uncoloredPixels;
    public List<int> uncoloredPixelsDummy;
    
    public Slider sliderCompletion;
    public int maxPixels = -1;

    //public Button restartButton;
    public static PaintCompletionDetection instance;

    [Header("Correction")]
    public Texture2D maskUv;
    public List<Vector2Int> uncolouredV2;
    public int comparisonSize;
    //public bool cleaning;
    public float cleaningFac;

    //public bool dirty;
    //public Color dirtyColor;
    //public Text testText;
    //public RenderTexture currentRT;
    //public Texture tex;
    //public Material mat1;
    //public float t;
    //public float newxtBell;

    public bool initialized;
    public float completionThreshold;
    public float waverCleanScore;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
        dirtMaskHandler = GetComponent<DirtMaskHandler>();
        //Invoke("DetectUncolouredPixels", 0.5f);
        

        //InitialTasks();
    }

    // Update is called once per frame
    void Update()
    {
        if (!initialized)
        {
            return;
        }

        //currentRT = (RenderTexture)DirtMaskHandler.instance.texWritten;

        //t = Time.time;
        //if(t > newxtBell)
        //{
        //    tex2dTest = toTexture2D(currentRT);
        //    DetectCompletionNew(tex2dTest, tex2dTest.width, comparisonSize);
        //
        //    newxtBell += 0.5f;
        //}
        //
        //Application.targetFrameRate = 100;

        //tex2dTest = TextureToTexture2D(DirtMaskHandler.instance.texWritten);
        DetectCompletionNew(tex2dTest, tex2dTest.width, comparisonSize);

        //tex2d = toTexture2D(RT);
        //colorArray = DetectPixels(tex2d);


        //if (!initialized)
        //{
        //    DetectUncolouredPixels();
        //    initialized = true;
        //
        //}
        
        

        //DetectCompletion();
    }

    //public  Texture2D ToTexture2D(Texture texture)
    //{
    //    return Texture2D.CreateExternalTexture(
    //        texture.width,
    //        texture.height,
    //        TextureFormat.RGB24,
    //        false, false,
    //        texture.GetNativeTexturePtr());
    //}
    //Texture2D toTexture2D(RenderTexture rTex)
    //{
    //    Texture2D tex = new Texture2D(rTex.width, rTex.height, TextureFormat.RGB24, false);
    //    RenderTexture.active = rTex;
    //    tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
    //    tex.Apply();
    //    
    //    return tex;
    //}

    //public Color[] DetectPixels(Texture2D tex)
    //{
    //    
    //    return tex.GetPixels();
    //    
    //    if (!initialized)
    //    {
    //        //DetectUncolouredPixels();
    //        
    //
    //    }
    //}

    public void DetectUncolouredPixels(Texture2D tex)
    {
        colorArray = tex.GetPixels();
        uncoloredPixels.Clear();
        for (int i = 0; i < colorArray.Length; i++)
        {
            if(colorArray[i] == Color.white)
            {
                uncoloredPixels.Add(i);
                //uncoloredPixelsDummy.Add(i);
            }
        }
        //if (maxPixels < 0)
        //{
        //    
        //}
        maxPixels = uncoloredPixels.Count;
        //initialized = true;
    }
    //public void DetectCompletion()
    //{
    //    //List<int> dummy = uncoloredPixels;
    //    uncoloredPixelsDummy.Clear();
    //    for (int i = 0; i < uncoloredPixels.Count; i++)
    //    {
    //        if (colorArray[uncoloredPixels[i]] == Color.white)
    //        {
    //            uncoloredPixelsDummy.Add(uncoloredPixels[i]);
    //        }
    //    }
    //
    //    uncoloredPixels = new List<int>(uncoloredPixelsDummy);
    //
    //
    //    //maxPixels = uncoloredPixels.Count;
    //    if (initialized)
    //    {
    //        sliderCompletion.value = 1 -(  (float)uncoloredPixels.Count / (float)maxPixels);
    //
    //        if(sliderCompletion.value >= 0.95)
    //        {
    //            restartButton.gameObject.SetActive(true);
    //        }
    //    }
    //    
    //}

    public void Restart()
    {
        Application.LoadLevel(0);
    }
    
    //private Texture2D TextureToTexture2D(Texture texture)
    //{
    //    Texture2D texture2D = new Texture2D(texture.width, texture.height, TextureFormat.RGBA32, false);
    //    RenderTexture currentRT = RenderTexture.active;
    //    RenderTexture renderTexture = RenderTexture.GetTemporary(texture.width, texture.height, 32);
    //    Graphics.Blit(texture, renderTexture);
    //
    //    RenderTexture.active = renderTexture;
    //    texture2D.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
    //    texture2D.Apply();
    //
    //    RenderTexture.active = currentRT;
    //    RenderTexture.ReleaseTemporary(renderTexture);
    //    return texture2D;
    //}

    public Texture2D ResizeDownTo(Texture2D sourceTex, int size)
    {
        if(size == 0)
        {
            return null;
        }
        if(size > sourceTex.width)
        {
            return sourceTex;
        }
        int downScale = sourceTex.width / size;
        Color[] sourceColorArray = sourceTex.GetPixels();
        Color[] outColorArray = new Color[size * size];
        uncolouredV2.Clear();
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                outColorArray[(i * size )+ j] = sourceColorArray[((i * size* downScale) + j) * downScale];
                if(outColorArray[(i * size) + j] == Color.white)
                {
                    uncolouredV2.Add(new Vector2Int(j, i));
                }    
                
            }
        }

        Texture2D OutTex = new Texture2D(size, size, TextureFormat.RGBA32, false);
        OutTex.SetPixels(outColorArray);
        OutTex.Apply();
        return OutTex;
    }

    public void DetectCompletionNew(Texture2D source , int sourceSize , int outputSize)
    {
        int downScale = sourceSize / outputSize;

        Vector2Int inBigArray = new Vector2Int((int) (RaycastHitDetector.instance.uvHitPos.x * (float)sourceSize), (int)(RaycastHitDetector.instance.uvHitPos.y * (float)sourceSize));
        //dirtyColor = source.GetPixel(inBigArray.x, inBigArray.y);
        Vector2Int intSmallArray = new Vector2Int((int)((float)inBigArray.x / (float)downScale), (int)((float)inBigArray.y / (float)downScale));
        //testText.text = RaycastHitDetector.instance.uvHitPos+""+inBigArray + "" + intSmallArray;
        //Debug.Log(intSmallArray);
        cleaningFac = Mathf.MoveTowards(cleaningFac, 0, Time.deltaTime);
        
        if (uncolouredV2.Contains(intSmallArray))
        {
            uncolouredV2.Remove(intSmallArray);
            cleaningFac = 1;
        }
        

        

        //List<Vector2Int> subList = new List<Vector2Int>();
        //colorArray = source.GetPixels();
        //for (int i = 0; i < uncolouredV2.Count; i++)
        //{
        //    //Color c = source.GetPixel(uncolouredV2[i].x * downScale, uncolouredV2[i].y*downScale);
        //    Color c = colorArray[(uncolouredV2[i].y * downScale * downScale * outputSize)+( uncolouredV2[i].x * downScale)];
        //    if (c.r == 1)
        //    {
        //        subList.Add(uncolouredV2[i]);
        //    } 
        //}
        //
        ////cleaning = false;
        //cleaningFac = Mathf.MoveTowards(cleaningFac, 0, Time.deltaTime);
        //if (uncolouredV2.Count != subList.Count)
        //{
        //    //cleaning = true;
        //    cleaningFac = 1;
        //}
        //
        //
        //
        //uncolouredV2.Clear();
        //uncolouredV2 = subList;

        float f = 1 - ((float)uncolouredV2.Count / (float)maxPixels);
        sliderCompletion.value = f + waverCleanScore;
        //Debug.Log(1 - ((float)uncolouredV2.Count / (float)maxPixels));

        if (sliderCompletion.value >= completionThreshold)
        {
            UiManage.instance.ButtonDonePaintingAppear();
        }

        if (sliderCompletion.value >= 1)
        {
            GameStatesControl.instance.GameStateChangeTo(GameStates.LevelResult);
        }
    }

    public void InitialTasks()
    {
        maskUv = ResizeDownTo(maskUv, comparisonSize);
        DetectUncolouredPixels(maskUv);

        initialized = true;
        waverCleanScore = 0;
    }


}
