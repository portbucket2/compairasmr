﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesBehaviorHandler : MonoBehaviour
{
    public ParticleSystem[] particlesToControl;
    public int[] basicEmissions;
    public bool half;
    public int emitt;
    public float emissionFac;
    public int currentEmit;
    public AudioSource audioSouce;
    float mul;

    // Start is called before the first frame update
    void Start()
    {
        basicEmissions = new int[particlesToControl.Length];
        for (int i = 0; i < basicEmissions. Length; i++)
        {
            var emit = particlesToControl[i].emissionRate;
            basicEmissions[i] = (int)emit;
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        emissionFac = PaintCompletionDetection.instance.cleaningFac;
        if(emissionFac > 0.99f)
        {
            if(audioSouce != null)
            {
                //audioSouce.Play();
            }
            //Debug.Log(emissionFac);
            audioSouce.volume = 0.1f;
            mul = 1;
        }
        mul = Mathf.MoveTowards(mul, 0, Time.deltaTime * 5f);
        audioSouce.volume = 0.1f *mul;
        //ManageParticleEmissions(Mathf.Clamp(emissionFac, 0.2f,1f));
        ManageParticleEmissions(emissionFac);


    }

    public void ManageParticleEmissions(float fac)
    {
        for (int i = 0; i < particlesToControl.Length; i++)
        {
            currentEmit = (int)(fac * (float)basicEmissions[i]);
            particlesToControl[i].emissionRate = currentEmit;
        }
    }
}
