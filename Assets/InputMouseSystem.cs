﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InputMouseSystem : MonoBehaviour
{
    public static InputMouseSystem instance;

    public Vector2 oldPos;
    public Vector2 newPos;
    public Vector2 mouseProgression;
    [SerializeField]
    bool tapped;

    public Action actionTouchIn;
    public Action actionTouchHolded;
    public Action actionTouchOut;
    [SerializeField]
    bool touchEnabled;


    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!touchEnabled)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            actionTouchIn?.Invoke();
        }
        if (Input.GetMouseButton(0))
        {
            tapped = true;
            oldPos = newPos;
            newPos = Input.mousePosition;
            mouseProgression = newPos - oldPos;

            actionTouchHolded?.Invoke();
        }
        else
        {
            tapped = false;
        }
        if (Input.GetMouseButtonUp(0))
        {
            actionTouchOut?.Invoke();
        }
    }

    public bool MouseTappedOrNot()
    {
        return tapped;
    }

    public bool TouchEnabledOrNot()
    {
        return touchEnabled;
    }

    public void TouchEnable()
    {
        touchEnabled = true;
    }
    public void TouchDisable()
    {
        touchEnabled = false;
        tapped = false;
        actionTouchOut?.Invoke();
    }
}
