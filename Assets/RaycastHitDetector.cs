﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RaycastHitDetector : MonoBehaviour
{
    public static RaycastHitDetector instance;

    public LayerMask tapHitLayer;
    public LayerMask levelMaskHitLayer;
    public ParticleSystem particlesOnHit;
    public GameObject ParticleHolder;
    public bool tapHitted;
    public bool particleOn;
    public Vector3 hitPointPos;
    public bool particleOnSusfaceNormal;
    public AudioSource audioSource;
    public float audioVolume;
    public GameObject particleHolderRootPosRef;

    public Vector2 uvHitPos;
    public bool uvHit;
    public bool levelMaskHitted;

    public Action actionMaskHitIn;
    public Action actionMaskHitOut;
    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        audioVolume = 0;
        audioSource.volume = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (InputMouseSystem.instance.MouseTappedOrNot())
        {
            RaycastAtClickedPos();
            RaycastForLevelMask();
        }
        else
        {
            if (levelMaskHitted)
            {
                actionMaskHitOut?.Invoke();
                levelMaskHitted = false;
            }
            
            tapHitted = false;
            ParticleHolder.transform.position = Vector3.Lerp(ParticleHolder.transform.position, particleHolderRootPosRef.transform.position, Time.deltaTime * 5f);
        }

        if (tapHitted)
        {
            if (!particleOn)
            {
                particlesOnHit.Play();
                audioVolume = 1f;
                particleOn = true;
            }
        }
        else
        {
            if (particleOn)
            {
                particlesOnHit.Stop();
                audioVolume = 0;
                particleOn = false;
            }
        }
        audioSource.volume = Mathf.MoveTowards (audioSource.volume , audioVolume  ,Time.deltaTime * 4f );

        //if (Input.GetMouseButtonDown(0))
        //{
        //    particlesOnHit.Play();
        //}
        //if (Input.GetMouseButtonUp(0))
        //{
        //    particlesOnHit.Stop();
        //}
    }

    public Vector3 RaycastAtClickedPos()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition + new Vector3(0, DirtMaskHandler.instance.mouseOffset)); // Construct a ray from the current mouse coordinates
        RaycastHit hit;
        Vector3 hitPos = new Vector3();

        //tapHitted = false;
        if (Physics.Raycast(ray, out hit, 200f, tapHitLayer))
        {
            if (uvHit)
            {
                uvHitPos = hit.textureCoord2;
                //hit.
            }
            //PaintCompletionDetection.instance.testText.text = "" + hit.textureCoord1;
            //Debug.DrawLine(hit.point, reflectVec, Color.red);
            //Debug.Log(hit.point +" "+ hit.transform.gameObject);
            //hitPos = new Vector3(hit.point.x, 0, hit.point.z);
            hitPointPos = hit.point;
            hitPos = hit.point;
            tapHitted = true;

            ParticleHolder.transform.position = hitPos;
            Quaternion rot = Quaternion.LookRotation(hit.normal);
            if (particleOnSusfaceNormal)
            {
                ParticleHolder.transform.rotation = Quaternion.Lerp(ParticleHolder.transform.rotation, rot, Time.deltaTime * 10f);
            }
            else
            {
                ParticleHolder.transform.rotation = Quaternion.Euler(new Vector3(-90, 0, 0));
            }
            

            //Debug.Log("hitClick " + hit.transform.gameObject);
            //LaserLineRendererFunction();
        }
        else
        {
            tapHitted = false;
            ParticleHolder.transform.position = Vector3.Lerp(ParticleHolder.transform.position, particleHolderRootPosRef.transform.position, Time.deltaTime * 2);
            //particlesOnHit.Stop();
        }

        return hitPos;
    }
    public void RaycastForLevelMask()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition + new Vector3(0, DirtMaskHandler.instance.mouseOffset)); // Construct a ray from the current mouse coordinates
        RaycastHit hit;
        //Vector3 hitPos = new Vector3();

        //tapHitted = false;
        if (Physics.Raycast(ray, out hit, 200f, levelMaskHitLayer))
        {
            if (!levelMaskHitted)
            {
                actionMaskHitIn?.Invoke();
                levelMaskHitted = true;
            }
            
        }
        else
        {
            if (levelMaskHitted)
            {
                actionMaskHitOut?.Invoke();
                levelMaskHitted = false;
            }
            
        }

        
    }
}
