﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;
    public LevelHolder[] levelHolders;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LevelStart()
    {
        for (int i = 0; i < levelHolders.Length; i++)
        {
            if(i == GameManagement.instance.GetLevelIndex())
            {
                levelHolders[i].gameObject.SetActive(true);
            }
            else
            {
                levelHolders[i].gameObject.SetActive(false);
            }
        }
        levelHolders[GameManagement.instance.GetLevelIndex()].gameObject.SetActive(true);

        levelHolders[GameManagement.instance.GetLevelIndex()].InitializeLevel();
    }

    public void LevelResult()
    {

    }
    public void LevelEnd()
    {

    }
}
