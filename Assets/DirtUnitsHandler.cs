﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirtUnitsHandler : MonoBehaviour
{
    public Vector3 range;
    public GameObject dirtUnit;
    public int dirtCount;
    public List<DirtUnit> dirtList;
    // Start is called before the first frame update
    void Start()
    {
        CreateDirts();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreateDirts()
    {
        for (int i = 0; i < dirtCount; i++)
        {
            Vector3 pos = new Vector3(Random.Range(-range.x, range.x), Random.Range(-range.y, range.y), Random.Range(-range.z, range.z));
            Quaternion rot = Quaternion.Euler(new Vector3(0, Random.Range(0, 90), 0));

            GameObject go = Instantiate(dirtUnit, pos, rot);
            go.transform.SetParent(transform);
            dirtList.Add(go.GetComponent<DirtUnit>());
        }

        

    }
}
