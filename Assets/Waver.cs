﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waver : MonoBehaviour
{
    public GameObject unitWaver;
    public GameObject[] units;
    public float speed;
    public float[] a;
    public float offset;
    public float maxAngle;
    float target;
    public float directionFacX;
    public float directionFacZ;
    public GameObject brush;
    public GameObject holder;
    
    public Vector3 brushLocal;
    public float angleee;
    public float initialOffset;
    public float eliminationTimer;
    public float lifeTime;

    public bool blown;
    // Start is called before the first frame update
    void Start()
    {
        initialOffset = Random.Range(0, 1f);
        speed *= (1 + Random.Range(-0.2f, 0.5f));
        lifeTime *= (1 + Random.Range(-0.2f, 0.2f));
        maxAngle *= (1 + Random.Range(-0.5f, 0.5f));
    }

    // Update is called once per frame
    void Update()
    {
        if (blown)
        {
            //return;
        }
        WaveIt();

        //Holder.transform.rotation = Quaternion.LookRotation(brush.transform.position - transform.position, transform.up);

        //units[0].transform.localRotation = Quaternion.Euler(a * 30f, 0, 0);
    }

    public  void  WaveIt()
    {
        float brushDis = Vector3.Distance(transform.position, brush.transform.position);
        if (RaycastHitDetector.instance.tapHitted && brushDis< 0.1f)
        {
            for (int i = 0; i < units.Length; i++)
            {
                a[i] = Mathf.Sin(((Time.time+ initialOffset) * speed) + (offset * i)) * maxAngle;
                //units[i].transform.localRotation = Quaternion.Euler(a[i], 0, 0);
            }
            eliminationTimer += Time.deltaTime;
            if(eliminationTimer > lifeTime)
            {
                if (!blown)
                {
                    KnockOut();
                }
                
            }
        }
        else
        {
            if(eliminationTimer != 0)
            {
                eliminationTimer = 0;
            }
            
            if (a[0] > 0)
            {
                a[0] = 20;
            }
            else
            {
                a[0] = -20;
            }
            for (int i = 0; i < units.Length; i++)
            {
                a[i] = a[0] * ((i * 0.5f) + 1);
                //units[i].transform.localRotation = Quaternion.Euler(a[i], 0, 0);
            }
        }

        for (int i = 0; i < units.Length; i++)
        {

            units[i].transform.localRotation = Quaternion.Lerp(units[i].transform.localRotation, Quaternion.Euler(a[i] , a[i] , 0), Time.deltaTime * 20);
           // units[i].transform.localRotation = Quaternion.Lerp(units[i].transform.localRotation, Quaternion.Euler(a[i] * directionFacX, a[i] * directionFacZ, 0), Time.deltaTime * 20);
        }
        if(brushDis< 0.3f)
        {
            holder.transform.localRotation = Quaternion.Lerp(holder.transform.localRotation, Quaternion.Euler(50 * -directionFacX, 50 * -directionFacZ, 0), Time.deltaTime * 20);
        }

        
        brushLocal = transform.InverseTransformPoint(brush.transform.position);
        angleee = (Vector3.SignedAngle(transform.forward, brush.transform.position - transform.position, transform.up) * Mathf.Deg2Rad);
        directionFacZ = Mathf.Sin(angleee);
        directionFacX = Mathf.Cos(angleee);

        if (RaycastHitDetector.instance.tapHitted)
        {
            brush.transform.position = new Vector3( RaycastHitDetector.instance.hitPointPos.x , transform.position.y, RaycastHitDetector.instance.hitPointPos.z);
        }

        //if (Input.GetKeyDown(KeyCode.K))
        //{
        //    KnockOut();
        //}
    }

    public void KnockOut()
    {
        this.gameObject.AddComponent<Rigidbody>();
        this.GetComponent<Rigidbody>().AddForce(holder.transform.forward * 200f + transform.up *300f);
        this.GetComponent<Rigidbody>().AddTorque(Vector3.one * Random.Range(-100f , 300f));
        Destroy(this.gameObject, 1f);


        PaintCompletionDetection.instance.waverCleanScore += 0.03f;

        //GameObject go = Instantiate(holder.gameObject, holder.transform.position, holder.transform.rotation);
        ////GameObject go = Instantiate(this.gameObject, transform.position, transform.rotation);
        //go.transform.SetParent(transform);
        //go.transform.localScale = Vector3.one;
        //holder.SetActive(false);
        //
        //go.gameObject.AddComponent<Rigidbody>();
        //go.GetComponent<Rigidbody>().AddForce(holder.transform.forward * 50f);
        //Destroy(go, 2f);

        blown = true;


    }

    public void ResetWaver()
    {
        blown = false;
        holder.SetActive(true);
    }
}
