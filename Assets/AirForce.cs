﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirForce : MonoBehaviour
{
    public Rigidbody[] rb;
    public float f;
    public float mul;
    public AnimationCurve cur;
    public float fr;
    // Start is called before the first frame update
    void Start()
    {
        //CallChange();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            foreach (var item in rb)
            {
                float a = Vector3.Angle(transform.up, item.gameObject.transform.forward);
                item.AddForce(Vector3.up * f*cur.Evaluate(mul)* a/180f *Mathf.Sin(Time.deltaTime*50f));
            }
            
        }
        mul += Time.deltaTime*fr;
        if (mul > 1)
        {
            mul -= 1;
        }
    }
    public void CallChange()
    {
        Change();
        Invoke("CallChange", 0.8f);
    }
    public void Change()
    {
        //mul = Random.Range(0.5f, 2f);
        mul = 0;
    }
}
