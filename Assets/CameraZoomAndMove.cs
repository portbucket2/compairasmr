﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoomAndMove : MonoBehaviour
{
    public GameObject camPivot;
    public GameObject camHolder;
    public GameObject camPosRef;

    public Vector3 tarPos;
    public Vector3 hitPoint;
    public Vector2 moveSpeed;
    bool clicked;
    bool hitted;
    public LayerMask tapHitLayer;
    public GameObject clickObj;
    public float camZoomFac;
    bool sideMoveAllowed;
    // Start is called before the first frame update
    void Start()
    {
        tarPos = camPosRef.transform.position;

        InputMouseSystem.instance.actionTouchIn += MouseClickInFun;
        InputMouseSystem.instance.actionTouchOut += MouseClickOutFun;

        RaycastHitDetector.instance.actionMaskHitIn += SideMoveEnable;
        RaycastHitDetector.instance.actionMaskHitOut += SideMoveDisable;

    }

    // Update is called once per frame
    void Update()
    {
        CamPivotMove();
    }
    public void CamPivotMove()
    {
        if (RaycastHitDetector.instance != null)
        {
            //hitPoint = RaycastHitDetector.instance.hitPointPos;
            //if (!clicked)
            //{
            //    tarPos = hitPoint;
            //    clicked = true;
            //}
        }

        if (Input.GetMouseButtonDown(0))
        {
            //tarPos = hitPoint;


            //Vector3 hitPos = RaycastAtClickedPos();
            //
            //
            //if (clickObj != null)
            //{
            //    tarPos = hitPos;
            //    tarPos = Vector3.Lerp(camHolder.transform.position, tarPos, camZoomFac);
            //}
            
            
        }

        if(clickObj != null)
        {

        }
        if (InputMouseSystem.instance.MouseTappedOrNot())
        {
            if(Input.mousePosition.x > Screen.width * 0.9f)
            {
                moveSpeed.x = -0.1f;
            }
            else if (Input.mousePosition.x < Screen.width * 0.1f)
            {
                moveSpeed.x = 0.1f;
            }
            else
            {
                moveSpeed.x = 0;
            }
            if (Input.mousePosition.y > Screen.height * 0.7f)
            {
                moveSpeed.y = -0.1f;
            }
            else if (Input.mousePosition.y < Screen.height * 0.1f)
            {
                moveSpeed.y = 0.1f;
            }
            else
            {
                moveSpeed.y = 0;
            }

            //Vector3 hitPos = RaycastAtClickedPos();
            if (clickObj == null)
            {
                Vector3 hitPos = RaycastAtClickedPos();


                if (clickObj != null)
                {
                    tarPos = hitPos;
                    tarPos = Vector3.Lerp(camHolder.transform.position, tarPos, camZoomFac);
                }
            }

            if ( clickObj != null)
            {

                if (!sideMoveAllowed)
                {
                    return;
                }

                tarPos += new Vector3(moveSpeed.x, 0, moveSpeed.y) * Time.deltaTime * 2;
                //camHolder.transform.position = Vector3.Lerp(camHolder.transform.position, tarPos, Time.deltaTime * 2f);
            }
            else
            {
                
                //camHolder.transform.position = Vector3.Lerp(camHolder.transform.position, tarPos, Time.deltaTime * 2f);
            }
        }
        else
        {
            moveSpeed = Vector2.zero;
            //camHolder.transform.position = Vector3.Lerp(camHolder.transform.position, tarPos, Time.deltaTime * 2f);
        }
        camHolder.transform.position = Vector3.Lerp(camHolder.transform.position, tarPos, Time.deltaTime *8f);
        if (Input.GetMouseButtonUp(0))
        {
            //tarPos = camPosRef.transform.position;
            //clickObj = null;
        }



    }

    public Vector3 RaycastAtClickedPos()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition + new Vector3(0, DirtMaskHandler.instance.mouseOffset)); // Construct a ray from the current mouse coordinates
        RaycastHit hit;
        Vector3 hitPos = new Vector3();

        //tapHitted = false;
        if (Physics.Raycast(ray, out hit, 200f, tapHitLayer))
        {

            //Debug.DrawLine(hit.point, reflectVec, Color.red);
            //Debug.Log(hit.point +" "+ hit.transform.gameObject);
            //hitPos = new Vector3(hit.point.x, 0, hit.point.z);
            //hitPointPos = hit.point;
            hitPos = hit.point;
            clickObj = hit.transform.gameObject;
            hitted = true;
            //tapHitted = true;

            //ParticleHolder.transform.position = hitPos;
            //Quaternion rot = Quaternion.LookRotation(hit.normal);
            


            //Debug.Log("hitClick " + hit.transform.gameObject);
            //LaserLineRendererFunction();
        }
        else
        {
            hitted = false;
            clickObj = null;
            //tapHitted = false;
            //particlesOnHit.Stop();
        }

        return hitPos;
    }

    public void MouseClickInFun()
    {
        Vector3 hitPos = RaycastAtClickedPos();


        if (clickObj != null)
        {
            tarPos = hitPos;
            tarPos = Vector3.Lerp(camHolder.transform.position, tarPos, camZoomFac);
        }
    }
    public void MouseClickHoldedFun()
    {

    }
    public void MouseClickOutFun()
    {
        tarPos = camPosRef.transform.position;
        //clicked = false;
        //hitted = false;
        clickObj = null;
    }
    public void SideMoveEnable()
    {
        sideMoveAllowed = true;
    }
    public void SideMoveDisable()
    {
        sideMoveAllowed = false;
    }
}
