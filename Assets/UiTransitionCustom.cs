﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UiTransitionCustom : MonoBehaviour
{
    [Header("InitialValues")]
    public Canvas parentPanel;
    public Vector2 refResolution;
    public float originalWidthFarctionToHeight;
    public float currentWidthFarctionToHeight;
    public float uiWidth;
    public float uiHeight;
    [Header("TransitionValues")]
    Vector3 centerPos;

    public AnimationCurve fastTransitionCurve;

    Vector3 from;
    Vector3 to;

    public enum UiTransitionTypeCustom
    {
        LeftToCenter,RightToCenter,CenterToLeft,  CenterToRight  
    }
    // Start is called before the first frame update
    void Start()
    {
        //InitialDataGrabbing();



    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            TransitionUpOrDownToCenter(-3000,0);
        }
    }
    void InitialDataGrabbing()
    {
        parentPanel = GetComponentInParent<Canvas>();
        refResolution = parentPanel.GetComponent<CanvasScaler>().referenceResolution;
        originalWidthFarctionToHeight = (float)refResolution.x / (float)refResolution.y;
        currentWidthFarctionToHeight = (float)Screen.width / (float)Screen.height;
        uiWidth = (currentWidthFarctionToHeight / originalWidthFarctionToHeight) * refResolution.x;
        uiHeight = refResolution.y;
    }

    public IEnumerator TransitionMove(Vector3 from, Vector3 to , float progress, float delay = 0, float speed = 1, Action act = null, bool justDeactive = false)
    {
        if(progress == 0)
        {
            yield return new WaitForSeconds(delay);
        }
        
        yield return new WaitForSeconds(0);
        if (progress >= 1)
        {
            progress = 1;
            transform.localPosition = Vector3.Lerp(from, to, fastTransitionCurve.Evaluate(progress));
            act?.Invoke();
            if (justDeactive)
            {
                transform.localPosition =  from;
                this.gameObject.SetActive(false);
            }
        }
        else
        {
            progress += Time.deltaTime * speed;
            //progress += .01f * speed;
            //Debug.Log(progress);
            StartCoroutine( TransitionMove(from, to, progress, delay,speed,act, justDeactive));
            transform.localPosition = Vector3.Lerp(from, to, fastTransitionCurve.Evaluate(progress));
        }
        

    }

    public void TransitionUpOrDownToCenter(float Offset , float delay = 0, float speed = 1,  Action act = null)
    {
        transform.localPosition = new Vector3(0, Offset, 0);
        StartCoroutine(TransitionMove(new Vector3(0,Offset, 0), new Vector3(0, 0, 0), 0, delay,speed, act));
    }
    public void TransitionRightOrLeftToCenter(float OffsetFrom , float OffsetTo, float delay = 0, float speed = 1, Action act = null, bool justDeactivate= false)
    {
        transform.localPosition = new Vector3( OffsetFrom,0, 0);
        StartCoroutine(TransitionMove(new Vector3(OffsetFrom, 0, 0), new Vector3(OffsetTo, 0, 0), 0, delay, speed, act, justDeactivate));
    }
}
