﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisablePaintingOfPaint3D : MonoBehaviour
{
    
    // Start is called before the first frame update
    void Start()
    {
        //PaintingDisable();
        //GameStatesControl.instance.actionLevelStart +=  PaintingEnable;
        GameStatesControl.instance.actionLevelStart += PaintingDisable;
        GameStatesControl.instance.actionLevelResult += PaintingDisable;

        RaycastHitDetector.instance.actionMaskHitIn += PaintingEnable;
        RaycastHitDetector.instance.actionMaskHitOut += PaintingDisable;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PaintingDisable()
    {
        GetComponent<LevelManager>().levelHolders[GameManagement.instance.GetLevelIndex()].meshToBePainted.SetActive(false);
    }
    public void PaintingEnable()
    {
        GetComponent<LevelManager>().levelHolders[GameManagement.instance.GetLevelIndex()].meshToBePainted.SetActive(true);
    }
}
