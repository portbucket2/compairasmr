﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DelayedAction : MonoBehaviour
{

    public Action myAction;
    public Delegate d;
    // Start is called before the first frame update
    void Start()
    {
        //FuntionAssign(d = new Delegate(MyFun));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MyFun()
    {

    }
    public void FuntionAssign(Delegate d)
    {
        d?.DynamicInvoke ();
    }
}
