﻿public enum GameStates
{
    MenuIdle    ,
    GameStart    ,
    LevelStart    ,
    Gameplay        ,
    LevelResult     ,
    LevelEnd,
    CallNextLevel
}

public enum GameplaySubState
{
    Idle, TouchIn, TouchHolded , TouchOut 
}
