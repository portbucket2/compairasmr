﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManage : MonoBehaviour
{
    public GameObject PanelMenuIdle   ;
    public GameObject PanelLevelResult;
    public GameObject PanelLevelEnd   ;

    private List<GameObject> allPanels;
    public static UiManage instance;

    public Button buttonDonePainting;

    [Header("ValuesToUpdate")]
    public Text textLevelValue;
    public Text textCleanValue;

    private void Awake()
    {
        instance = this;
        allPanels = new List<GameObject>();
        allPanels.Clear();
        allPanels.Add(PanelMenuIdle   );
        allPanels.Add(PanelLevelResult);
        allPanels.Add(PanelLevelEnd);

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FunUiMenuIdle    ()
    {
        AllPanelsClose();
        PanelMenuIdle.SetActive(true);
    }
    public void FunUiGameStart   ()
    {
        AllPanelsClose();
    }
    public void FunUiLevelStart  ()
    {
        UiUpdate();
        AllPanelsClose();
    }
    public void FunUiGameplay    ()
    {
        AllPanelsClose();
    }
    public void FunUiLevelResult ()
    {
        AllPanelsClose();
        UiUpdate();
        PanelLevelResult.SetActive(true);
    }
    public void FunUiLevelEnd()
    {
        AllPanelsClose();
        PanelLevelEnd.SetActive(true);
    }


    #region( "ButtonFuntions")

    public void ButtonTapToStartFun()
    {

        GameStatesControl.instance.GameStateChangeTo(GameStates.GameStart);
    }
    public void ButtonNextLevelFun()
    {
        GameStatesControl.instance.GameStateChangeTo(GameStates.CallNextLevel);
    }
    public void ButtonDonePaintingFun()
    {

        GameStatesControl.instance.GameStateChangeTo(GameStates.LevelResult);
    }


    #endregion

    public void AllPanelsClose()
    {
        for (int i = 0; i < allPanels.Count; i++)
        {
            allPanels[i].SetActive(false);
        }
        buttonDonePainting.gameObject.SetActive(false);
    }
    public void ButtonDonePaintingAppear()
    {
        buttonDonePainting.gameObject.SetActive(true);
    }
    public void UiUpdate()
    {
        textLevelValue.text =""+ (GameManagement.instance.levelIndexDisplayed + 1);
        textCleanValue.text = "" + (int) (PaintCompletionDetection.instance.sliderCompletion.value * 100f) + " %";
    }
}
